# pokeAPI - sebafonseca



## Getting started

This is a django (v4.0) project hosted with Heroku.

In this project we retrieve data from [pokeapi](https://pokeapi.co/docs/v2#berries) and display some statistics.

All the required libraries are described in the [requirements.txt](https://gitlab.com/sebafonseca/pokeapi/-/blob/master/globant_excercise/requirements.txt) file.

### Running Locally
Under `globant_excercise` folder, you can activate the virtualenv by `source venv/bin/activate` and then
`pip install -r requirements.txt` for install all the required libraries.

You also need to run the migrations if you are running locally (makemigrations & migrate).

After that you are able to runserver => `python manage.py runserver` 

or run any test => `python manage.py test`

### Available endpoints
We have 3 available endpoints in this project:

- GET https://sebafonseca-pokeapi-globant.herokuapp.com/allBerryStats/
  - This will retrieve all data from [pokeapi](https://pokeapi.co/docs/v2#berries) and generates a json with the following structure
   
           { 
                 "berries_names": [...],
                 "min_growth_time": "" // time, int
                 "median_growth_time": "", // time, float
                 "max_growth_time": "" // time, int
                 "variance_growth_time": "" // time, float
                 "mean_growth_time": "", // time, float
                 "frequency_growth_time": "", // time, {growth_time: frequency, ...}
           }
  - If our local database is empty, we will call to pokeapi and save the result in our database. And the following requests to this endpoint will query our local database.

- GET https://sebafonseca-pokeapi-globant.herokuapp.com/refreshBerryStats/
  - If we want to refresh our local database with the data from [pokeapi](https://pokeapi.co/docs/v2#berries), we need to
  call this endpoint. The expected response is:
    
           {
                 "message": "All Berry data is up to date!. Try calling allBerryStats endpoint.",
                 "status code": 200
           }
  
- GET https://sebafonseca-pokeapi-globant.herokuapp.com/getPlot/
  - This will generate a Histogram graph (using matplotlib) and display the image in a plain html.

### Tests
We can run tests by running `python manage.py test`