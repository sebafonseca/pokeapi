from django.http import HttpResponse
from django.http import HttpResponseNotAllowed
from django.http import HttpResponseServerError
from .models import BerriesStatics
from django.views.decorators.csrf import csrf_exempt
import json
from django.shortcuts import render
from .utils import get_plot


@csrf_exempt
def get_all_berry_stats(request):
    if request.method == 'GET':
        try:
            berries_statics = BerriesStatics()
            berries_statics_json = berries_statics.to_json()
            return HttpResponse(berries_statics_json, content_type='application/json')
        except:
            return HttpResponseServerError(content_type='application/json',
                                           content=json.dumps({'message': 'Server Error',
                                                               'status code': 500}))
    else:
        return HttpResponseNotAllowed(permitted_methods='GET', content_type='application/json',
                                      content=json.dumps({'message': 'Method not allowed',
                                                          'status code': 405}))


@csrf_exempt
def get_refresh_berry_stats(request):
    if request.method == 'GET':
        try:
            BerriesStatics.load_dataset()
            return HttpResponse(
                json.dumps({'message': 'All Berry data is up to date!. Try calling allBerryStats endpoint.',
                            'status code': 200}), content_type='application/json')
        except:
            return HttpResponseServerError(content_type='application/json',
                                           content=json.dumps({'message': 'Server Error',
                                                               'status code': 500}))
    else:
        return HttpResponseNotAllowed(permitted_methods='GET', content_type='application/json',
                                      content=json.dumps({'message': 'Method not allowed',
                                                          'status code': 405}))


def plot_view(request):
    berries_statics = BerriesStatics()
    x_values = []
    y_values = []
    for elem in berries_statics.frequency_growth_time:
        x_values.append(elem['growth_time'])
        y_values.append(elem['frequency'])
    chart = get_plot(x_values, y_values)
    return render(request, 'pokeapi/templates/main.html', {'chart': chart})
