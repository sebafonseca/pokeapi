import requests


def get_berries_from_api():
	berry_urls = get_page_berries('https://pokeapi.co/api/v2/berry/', [])
	result = []
	for berry_url in berry_urls:
		response = requests.get(berry_url)
		if response.status_code == 200:
			data = response.json()
			growth_time = data['growth_time']
			name = data['name']
			result.append({'name': name, 'growth_time': growth_time})
			print(f"Name: {name} - Growth Time: {growth_time}")
	return result


def get_page_berries(url, berries):
	response = requests.get(url)
	if response.status_code == 200:
		data = response.json()
		next_url = data['next']
		for berry_data in data['results']:
			berries.append(berry_data['url'])
		if next_url:
			get_page_berries(next_url, berries)
		return berries
	else:
		raise Exception
