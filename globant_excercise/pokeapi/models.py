from django.db import models
from django.db.models import Min, Max
import json
import statistics
import collections
from .helper import get_berries_from_api


class Berry(models.Model):
    name = models.CharField(max_length=50, unique=True)
    growth_time = models.PositiveSmallIntegerField(null=False)


class BerriesStatics:

    def __init__(self):
        if self.is_empty_dataset():
            self.load_dataset()
        self.berries_names = list(Berry.objects.values_list('name', flat=True).all())
        self.min_growth_time = Berry.objects.all().aggregate(Min('growth_time'))['growth_time__min']

        growth_time_list = list(Berry.objects.values_list('growth_time', flat=True).all())

        self.median_growth_time = self.get_median(growth_time_list)
        self.max_growth_time = Berry.objects.all().aggregate(Max('growth_time'))['growth_time__max']
        self.variance_growth_time = self.get_variance(growth_time_list)
        self.mean_growth_time = self.get_mean(growth_time_list)
        self.frequency_growth_time = self.get_frequency(growth_time_list)

    @staticmethod
    def is_empty_dataset():
        return len(Berry.objects.all()) == 0

    @staticmethod
    def get_median(growth_time_list):
        return statistics.median(growth_time_list)

    @staticmethod
    def get_variance(growth_time_list):
        return statistics.variance(growth_time_list)

    @staticmethod
    def get_mean(growth_time_list):
        return statistics.mean(growth_time_list)

    @staticmethod
    def get_frequency(growth_time_list):
        result = []
        freq_count = collections.Counter(growth_time_list)
        for key, value in sorted(freq_count.items()):
            result.append({'growth_time': key, 'frequency': value})
        return result

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    @staticmethod
    def load_dataset():
        # Remove old data
        Berry.objects.all().delete()
        # Insert new data
        for berry in get_berries_from_api():
            current_berry = Berry(name=berry['name'], growth_time=berry['growth_time'])
            current_berry.save()
