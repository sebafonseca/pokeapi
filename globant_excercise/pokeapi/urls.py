from django.urls import path

from . import views

urlpatterns = [
    path('allBerryStats/', views.get_all_berry_stats),
    path('refreshBerryStats/', views.get_refresh_berry_stats),
    path('getPlot/', views.plot_view),
]
