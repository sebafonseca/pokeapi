from django.test import TestCase
from .models import Berry
from .models import BerriesStatics
import json


class BerryStaticsTestCase(TestCase):

    def setUp(self):
        Berry.objects.create(name='Berry1', growth_time=1)
        Berry.objects.create(name='Berry2', growth_time=1)
        Berry.objects.create(name='Berry3', growth_time=2)
        Berry.objects.create(name='Berry4', growth_time=2)
        Berry.objects.create(name='Berry5', growth_time=2)
        self.berriesStatics = BerriesStatics()

    def test_berries_statics(self):
        """Test if all the data from Berry objects is correctly generated in BerriesStatics object"""
        self.assertCountEqual(self.berriesStatics.berries_names, ["Berry1", "Berry2", "Berry3", "Berry4", "Berry5"])
        self.assertEqual(self.berriesStatics.min_growth_time, 1)
        self.assertEqual(self.berriesStatics.median_growth_time, 2)
        self.assertEqual(self.berriesStatics.max_growth_time, 2)
        self.assertEqual(self.berriesStatics.variance_growth_time, 0.3)
        self.assertEqual(self.berriesStatics.mean_growth_time, 1.6)
        self.assertJSONEqual(json.dumps(self.berriesStatics.frequency_growth_time[0]),
                             json.dumps({'growth_time': 1, 'frequency': 2}))
        self.assertJSONEqual(json.dumps(self.berriesStatics.frequency_growth_time[1]),
                             json.dumps({'growth_time': 2, 'frequency': 3}))


class ViewsTestCase(TestCase):

    def test_get_all_berry_stats(self):
        response = self.client.get('/allBerryStats/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertGreater(len(response.json()['berries_names']), 0)

    def test_get_refresh_berry_stats(self):
        self.assertEqual(len(Berry.objects.all()), 0)
        response = self.client.get('/refreshBerryStats/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertGreater(len(Berry.objects.all()), 0)
